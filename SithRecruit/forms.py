from django import forms

from SithRecruit.models import Recruit


class RecruitForm(forms.ModelForm):
    class Meta:
        model = Recruit
        fields = ['planet', 'name', 'age', 'email']

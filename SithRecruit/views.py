from django.core.mail import send_mail
from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from SithRecruit.forms import RecruitForm
from SithRecruit.models import Sith, Test, Recruit, Answer, Planet
from SithRecruitSystem.settings import EMAIL_HOST_USER


def recruit(request):
    if request.method == 'POST':
        form = RecruitForm(request.POST)
        if form.is_valid():
            rec = form.save()
            response = redirect('test')
            response.set_cookie('recruit_id', rec.id)
            return response
    else:
        if 'recruit_id' not in request.COOKIES:
            form = RecruitForm()
        else:
            if Recruit.objects.filter(pk=int(request.COOKIES['recruit_id'])).exists():
                form = RecruitForm(
                    initial=model_to_dict(Recruit.objects.get(pk=int(request.COOKIES['recruit_id']))))
            else:
                form = RecruitForm()

    return render(request, 'recruit.html', {'form': form})


def recruits_list(request):
    c = Sith.objects.filter(planet__recruit__master_sith_id=int(request.COOKIES['sith_id'])).count()

    planets = Planet.objects.all()
    planet_id = 0
    if 'planet' not in request.GET or int(request.GET['planet']) == 0:
        recruits = Recruit.objects.filter(master_sith__isnull=True)
    else:
        planet_id = int(request.GET['planet'])
        recruits = Recruit.objects.filter(planet_id=planet_id).filter(master_sith__isnull=True)

    return render(request, 'recruits_list.html',
                  {'recruits': recruits, 'planets': planets, 'planet_id': planet_id, 'count_of_recruits': c})


def test(request):
    if 'recruit_id' not in request.COOKIES:
        return HttpResponse('You are not recruit')
    else:
        rec_id = int(request.COOKIES['recruit_id'])
        rec = Recruit.objects.get(pk=rec_id)
        if rec.answers.count() == 0:
            if request.method == 'POST':
                test_id = str(request.POST['test_id'])
                questions = [x.id for x in Test.objects.get(pk=int(test_id)).questions.all()]
                answers_from_user = [int(x) for x in request.POST if x.isdigit()]
                final_answers = []
                for question_id in questions:
                    final_answers.append(
                        Answer.objects.create(question_id=question_id, answer=question_id in answers_from_user))

                rec = Recruit.objects.get(pk=rec_id)
                rec.answers.set(final_answers)
                rec.save()
                response = redirect('/thank_you')
                response.delete_cookie('recruit_id')
                return response
            else:
                test_for_recruit = Test.objects.get(pk=1)
                questions = test_for_recruit.questions.all()
                return render(request, 'test.html', {'questions': questions, 'test_id': test_for_recruit.id})
        else:
            return HttpResponse('Вы уже прошли испытание')


def thank_you(request):
    return render(request, 'thank_you.html')


def home(request):
    response = render(request, 'home.html')
    response.delete_cookie('sith_id')
    response.delete_cookie('recruit_id')
    return response


def results(request, recruit_id):
    sith_id = 0
    if 'sith_id' in request.COOKIES:
        sith_id = int(request.COOKIES['sith_id'])
    if request.method == 'POST':
        r = Recruit.objects.get(pk=recruit_id)
        count_of_recruits = Recruit.objects.filter(master_sith_id=sith_id).count()
        if count_of_recruits > 2:
            return HttpResponse('У Вас уже есть максимальное число рекрутов')
        r.master_sith = Sith.objects.get(pk=sith_id)
        r.save()
        try:
            send_mail('Набор в орден Ситхов',
                      'Вы успешно прошли испытание и зачислены Рукой Тени',
                      EMAIL_HOST_USER,
                      (r.email,))
        except Exception:
            return HttpResponse('Рекруту не отправлено сообщение о его зачислении. Ошибка почтовой службы')
        else:
            return HttpResponse('Рекруту отправлено сообщение о его зачислении')
    else:
        rec = Recruit.objects.get(pk=recruit_id)
        answers = rec.answers.all()
        count_of_recruits = Recruit.objects.filter(master_sith_id=sith_id).count()
        return render(request, 'results.html',
                      {'recruit': rec, 'answers': answers, 'count_of_recruits': count_of_recruits})


def sith(request):
    if request.method == 'POST':
        response = redirect('recruits_list')
        response.set_cookie('sith_id', request.POST['sith_id'])
        return response
    else:
        if 'shadow_hand_gt_1' in request.GET:
            siths = Sith.objects.raw(
                '''select * from (select sith.*, count(*) as recruit_count
                    from sith
                    join recruit r on sith.id = r.master_sith_id
                    group by sith.id) as s
                    where recruit_count > 1
                ''')
        else:
            siths = Sith.objects.all()
        return render(request, 'sith.html', {'siths': siths})

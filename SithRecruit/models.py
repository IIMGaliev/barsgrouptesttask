from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        db_table = 'planet'

    def __str__(self):
        return self.name


class Sith(models.Model):
    name = models.CharField(max_length=200)
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE)

    class Meta:
        db_table = 'sith'

    def __str__(self):
        return self.name


class Question(models.Model):
    text = models.TextField(default='')

    class Meta:
        db_table = 'question'

    def __str__(self):
        return self.text[:15]


class Test(models.Model):
    order_id = models.CharField(max_length=255, unique=True)
    questions = models.ManyToManyField(Question)

    class Meta:
        db_table = 'test'

    def __str__(self):
        return self.order_id[:15]


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.BooleanField()

    class Meta:
        db_table = 'answer'


class Recruit(models.Model):
    name = models.CharField(max_length=200)
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE)
    age = models.PositiveIntegerField()
    email = models.EmailField(unique=True)
    answers = models.ManyToManyField(Answer)
    master_sith = models.ForeignKey(Sith, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'recruit'

    def __str__(self):
        return self.name

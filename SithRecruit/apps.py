from django.apps import AppConfig


class SithrecruitConfig(AppConfig):
    name = 'SithRecruit'
